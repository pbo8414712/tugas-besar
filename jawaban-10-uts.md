# No.1

# No.2

# No.3

# No.4
Encapsulation is a technique to ensure that data is hidden from users. To do this, we have to set the attributes as private, Get method to access the private attributes, anda Set method to update value of the attributes (both methods are public). The benefits of Encapsulation are increase security of our program because user can't access the value of attributes directly and flexible to change a part of program without affect other codes.

```java
class Login
{
    String username;

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }
}

public class menu
{
    public static void main(String[]args)
    {
        Scanner input = new Scanner(System.in);
        int choose;
        Account account = new Account();
        Login user = new Login();

        System.out.print("Sign-in to continue\nUsername: ");
        account.username = input.next();
        user.setUsername(account.username);
        account.users.add(user.getUsername());
    }
}
```

# No.5
Abstraction is a technique of hide the details and only show the essential parts. It is used for classes and methods. We use abstract keyword for class and method. The benefits of this technique are our program is more secured because we hide the details like body of program and only write the essential parts like the title of methods.

```java

abstract class Transaction
{
    public abstract void explain();
    public abstract boolean purchase();
}

class eventBundle extends Transaction
{
    Scanner input = new Scanner(System.in);
    String confirm;

    public void explain()
    {
        System.out.println("This is Goblin Family's Party");
        System.out.println("Goblin Bundle: 1000 gems");
    }

    @Override
    public boolean purchase()
    {
        System.out.println("Are you sure want to purchase it?(Y/T)");
        confirm = input.next();
        boolean ending = false;

        if (confirm.equals("Y") || confirm.equals("y"))
        {
            ending = true;
        }
        else if (confirm.equals("T") || confirm.equals("t"))
        {
            ending = false;
        }
        return ending;
    }
}

```

# No.6
## Inheritance
Inheritance is a way to inherit attributes and methods from a class to another one. We inherit attributes and methods from super class or parent class to other class that called sub class or child class. To inherit them, we use extends keyword. The benefit of this is we don't have to rewrite the code like attributes and methods that have similar name.

```java

class profile
{
    Scanner input = new Scanner(System.in);
    String  name = "zalks",
            tag = "#9J8809UQL",
            clan = null,
            arena = "Training Camp",
            leagueCurrent = "Reach 5000+ trophies to get League stage.",
            deck,
            leaguePrev,
            leagueBest,
            favcard;
    int trophy, wins, threecrownwins, highestTrophies, cardsfound, totaldonations, warwins, clancardcollect, challengemaxwin, challengecardwon, tourplayed, tourwins;

    public profile()
    {
        System.out.println("Username: " + name);
        System.out.println("Tag: " + tag);
        System.out.println("Clan: + " + clan);
        System.out.println("Arena: " + arena);
        System.out.println("Trophy: " + trophy);
        System.out.println("Current League: " + leagueCurrent);
        System.out.println("Deck: ");
        System.out.println("Previous Season League: " + leaguePrev);
        System.out.println("Best League: " + leagueBest);
        System.out.println("---STATS ROYALE---");
    }
}

class Friends extends profile
{
    public void friends()
    {
        Friends friend1 = new Friends();
        friend1.name = "Ry4n";
        friend1.trophy = 4900;
        
        Friends friends2 = new Friends();
        friends2.name = "mArt1n";
        friends2.trophy = 4883;
    }
}

```

## Polymorphism
Polymorphism is a concept that allow us to create different body method which have same name to other methods. It is related to inheritance and abstraction. When we create a body of a method in child class, we must want it has different task with the method in parent class. The benefit of this is we can use the same method in different class without add a new method.

```java

abstract class Transaction
{
    public abstract void explain();
    public abstract boolean purchase();
}

class eventBundle extends Transaction
{
    Scanner input = new Scanner(System.in);
    String confirm;

    public void explain()
    {
        System.out.println("This is Goblin Family's Party");
        System.out.println("Goblin Bundle: 1000 gems");
    }

    @Override
    public boolean purchase()
    {
        System.out.println("Are you sure want to purchase it?(Y/T)");
        confirm = input.next();
        boolean ending = false;

        if (confirm.equals("Y") || confirm.equals("y"))
        {
            ending = true;
        }
        else if (confirm.equals("T") || confirm.equals("t"))
        {
            ending = false;
        }
        return ending;
    }
}

```

# No.7

# No.8
## Use Case
No | Use Case | Priority Rate
--- | --- | ---
1 | Player is able to login | 50
2 | Player is able to build up deck | 90
3 | Player is able to upgrade all cards | 80
4 | Player is able to buy items in Shop | 70
5 | Player is able to buy banner to decorate profile | 50
6 | Player is able to join and leave a clan | 70
7 | Player is able to create a clan | 70
8 | Player is able to play a battle against another player in Path of Legends Battle | 100
9 | Player is able to play a battle against another player in Trophy Road Battle | 100
10 | Player is able to play a battle against another player in Party Mode | 90
11 | Player is able to play a battle against computer in Training Camp | 80
12 | Player is able to play a battle against another player in current events | 80
13 | Player is able to play a battle against friend in Friendly Battle | 70
14 | Player is able to play a battle inside the clan | 70
15 | Player is able to see the Leaderboards | 70
16 | Player is able to see player profile | 70
17 | Player is able to read any news in News Royale | 60
18 | Player is able to change account in the settings | 60
19 | Player is able to add and delete friends in Soial menu | 60
20 | Player is able to chat and share emote in the clan | 60
21 | Player is able to trade cards with other members in the clan | 70
22 | Player is able to promote, demote, and kick clan's member (if you are the leader or co-leader) | 60 

# No.9

# No.10
