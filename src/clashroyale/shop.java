package clashroyale;

import java.util.Scanner;

public class shop
{
    public static void menushop()
    {
        Scanner input = new Scanner(System.in);
        int konfirmasi = 0;


        System.out.println("Which one do you want to purchase?");
        System.out.println("1. Pass Royale\n2. Event Bundle\n3. Trader\n4. Daily Deals\n5. Emotes");
        System.out.println("6. Royal Chests\n7. Gems\n8. Gold\n9. Creator Boost\n");
        System.out.print("Input here: ");
        konfirmasi = input.nextInt();

        switch (konfirmasi)
        {
            case 1:
                passRoyale passroyale = new passRoyale();
                passroyale.price = 79000;

                break;

            case 2:
                eventBundle goblin = new eventBundle();
                goblin.price = 1000;
                break;

            case 3:
                trader kartu = new trader();
                kartu.rollPrice = 10;
                break;

            case 4:
                //pembayaran menggunakan gold
                dailyDeals freeitem = new dailyDeals();
                freeitem.name = "Dart Goblin";
                freeitem.contain = 25;
                freeitem.price = 0;

                dailyDeals commonCard1 = new dailyDeals();
                commonCard1.name = "Zap";
                commonCard1.contain = 50;
                commonCard1.price = 500;

                dailyDeals commonCard2 = new dailyDeals();
                commonCard2.name = "Bomber";
                commonCard2.contain = 80;
                commonCard2.price = 800;

                dailyDeals rareCard = new dailyDeals();
                rareCard.name = "Inferno Tower";
                rareCard.contain = 40;
                rareCard.price = 4000;

                dailyDeals epicCard = new dailyDeals();
                epicCard.name = "X-Bow";
                epicCard.contain = 5;
                epicCard.price = 5000;

                dailyDeals legendaryCard = new dailyDeals();
                legendaryCard.name = "Inferno Dragon";
                legendaryCard.contain = 1;
                legendaryCard.price = 40000;

                break;

            case 5:
                emotes emote = new emotes();
                break;

            case 6:
                //pembayaran menggunakan gem
                royalChests chest1 = new royalChests();
                chest1.name = "Lighting Chest";
                chest1.price = 250;

                royalChests chest2 = new royalChests();
                chest2.name = "Fortune Chest";
                chest2.price = 750;

                royalChests chest3 = new royalChests();
                chest3.name = "Legendary King's Chest";
                chest3.price = 2500;

                break;

            case 7:
                //pembayaran menggunakan rupiah
                gems paket1 = new gems();
                paket1.name = "Fistful of Gems";
                paket1.contain = 80;
                paket1.price = 16000;

                gems paket2 = new gems();
                paket2.name = "Pouch of Gems";
                paket2.contain = 500;
                paket2.price = 79000;

                gems paket3 = new gems();
                paket3.name = "Bucket of Gems";
                paket3.contain = 1200;
                paket3.price = 159000;

                gems paket4 = new gems();
                paket4.name = "Barrel of Gems";
                paket4.contain = 2500;
                paket4.price = 329000;

                gems paket5 = new gems();
                paket5.name = "Wagon of Gems";
                paket5.contain = 6500;
                paket5.price = 799000;

                gems paket6 = new gems();
                paket6.name = "Mountain of Gems";
                paket6.contain = 14000;
                paket6.price = 1599000;

                break;

            case 8:
                //pembayaran menggunakan gem
                gold koin1 = new gold();
                koin1.name = "Pouch of Gold";
                koin1.contain = 1000;
                koin1.price = 60;

                gold koin2 = new gold();
                koin2.name = "Bucket of Gold";
                koin2.contain = 10000;
                koin2.price = 500;

                gold koin3 = new gold();
                koin3.name = "Wagon of Gold";
                koin3.contain = 100000;
                koin3.price = 4500;

                break;

            case 9:
                creatorBoost kode = new creatorBoost();
                System.out.print("Enter Creator Code: ");
                kode.code = input.next();
                break;

            default:
                System.out.println("Wrong Transaction!!!");
        }

    }
}

class passRoyale
{
    int price;

    public passRoyale()
    {
        System.out.println("You will get many benefits if you purchase Pass Royale:");
        System.out.println("- If you fail to accomplish an event, you can try agaim without limit access");
        System.out.println("- Get extra rewards when your mission is done");
    }
}

class eventBundle
{
    int price;
}

class specialOffer
{
    int price;
}

class trader
{
    String name;
    int contain;
    int rollPrice;
}

class dailyDeals
{
    String name;
    int contain;
    int price;
}

class emotes
{
    int price;
}

class royalChests
{
    String name;
    int price;
}

class gems
{
    String name;
    int contain;
    int price;
}

class gold
{
    String name;
    int contain;
    int price;
}

class creatorBoost
{
    String code;
}